import { Component } from '@angular/core';
import {LicenseManager} from "ag-grid-enterprise";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'customer-dashboard';
  constructor() {
		LicenseManager.setLicenseKey("Evaluation_License-_Not_For_Production_Valid_Until_26_June_2019__MTU2MTUwMzYwMDAwMA==69302cb98799e1ec2806d5ed4415071b");  
  }
  
}
