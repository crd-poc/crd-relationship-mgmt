package com.hcl.dfs.restapi.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.hcl.dfs.restapi.model.Customer;

public interface CustomerService {

	ResponseEntity<Object> deleteAll();
	ResponseEntity<Object> deleteByCustomerNumber(String id);
	ResponseEntity<List<Customer>> addCustomers(List<Customer> customers);
	ResponseEntity<List<Customer>> getAllCustomers();
	ResponseEntity<Customer> getByCustomerNumber(String customerId);
	ResponseEntity<List<Customer>> getAllLinkedCustomerNumbers(String customerNumber);
	
}
