import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyDetailComponent }  from '../company-detail/company-detail.component';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  private gridApi;
  private gridColumnApi;
  private gridParams;
	rowSelection = "single";
	customerId : number;

	columnDefs = [
      {headerName: 'Customer Name', field: 'customerName', sortable: true, filter: 'agSetColumnFilter' }, 
      {headerName: 'Customer No.', field: 'customerNumber', sortable: true, filter: 'agSetColumnFilter' },
      {headerName: 'Location', field: 'location', sortable: true, filter: 'agSetColumnFilter'},
      {headerName: 'Branch', field: 'branch', sortable: true, filter: 'agSetColumnFilter'},
      {headerName: 'Industry', field: 'industryName', sortable: true, filter: 'agSetColumnFilter'},
      {headerName: 'Relationship Type', field: 'relationship', sortable: true, filter: 'agSetColumnFilter'},
      {headerName: 'Subsidiary', field: 'subsidiary', sortable: true, filter: 'agSetColumnFilter'}
  ];

  rowData = [];

	onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridColumnApi = params.columnApi;
    setTimeout(() =>{
			this.gridApi.forEachNode((node) => {
				if (node.rowIndex === 0) 
					node.setSelected(true);
			})
    },500);
  }

  onSelectionChanged() {
    var selectedRows = this.gridApi.getSelectedRows();
    this.customerId=selectedRows[0].customerNumber;
    console.log("row clicked",selectedRows)    
  }

  constructor(
  	private route : ActivatedRoute,
  	private customerService: CustomerService
  ) { 
  	const id = +this.route.snapshot.paramMap.get('id');
  	this.getLinkedCustomers(id);
  	
  }
  
  ngOnInit() {
  }
  
  getLinkedCustomers(id): void {
    this.customerService.getLinkedCustomers(id)
      .subscribe(customers => {
      	this.rowData = customers?customers:[];
      	if(this.rowData.length>0)
      	this.customerId=this.rowData[0].no;
      });
  }  

}
