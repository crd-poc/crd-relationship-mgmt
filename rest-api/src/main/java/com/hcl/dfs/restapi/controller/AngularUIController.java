package com.hcl.dfs.restapi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class AngularUIController {

	@RequestMapping(value = "/dashboard/**", method = RequestMethod.GET)
	public String getDashboard(){
		return "forward:/";
	}
}
