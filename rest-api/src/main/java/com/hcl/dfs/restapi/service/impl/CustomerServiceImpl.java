package com.hcl.dfs.restapi.service.impl;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.hcl.dfs.restapi.model.Customer;
import com.hcl.dfs.restapi.repo.CustomerRepo;
import com.hcl.dfs.restapi.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomerRepo customerRepo;

	@Override
	public ResponseEntity<Object> deleteAll() {
		customerRepo.deleteAll();
		return ResponseEntity.ok(HttpStatus.NO_CONTENT);
	}

	@Override
	public ResponseEntity<List<Customer>> addCustomers(List<Customer> customers) {
		List<Customer> listOfSavedCustomers = new ArrayList<Customer>();
		for(Customer item : customers){
			if (!StringUtils.isEmpty(item.getCustomerNumber())){
				item.setCreatedAt(Instant.now());
			}
			Customer savedCustomer = customerRepo.save(item);
			listOfSavedCustomers.add(savedCustomer);
		}
		
		URI customerLocation = fromUriString("/customer/all").build().toUri();
		return ResponseEntity.created(customerLocation).body(listOfSavedCustomers);
	}

	@Override
	public ResponseEntity<List<Customer>> getAllCustomers() {
		List<Customer> customerList = customerRepo.findAll();
		if (log.isDebugEnabled()){
			for (Customer customer : customerList) {
				log.debug(customer.toString());
			}
		}
		return ResponseEntity.ok(customerList);
	}

	@Override
	public ResponseEntity<Customer> getByCustomerNumber(String customerNumber) {
		Optional<Customer> customer = customerRepo.findById(customerNumber);
		return customer.isPresent() ? ResponseEntity.ok(customer.get()) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@Override
	public ResponseEntity<List<Customer>> getAllLinkedCustomerNumbers(String customerNumber) {
		List<Customer> allLinkedCustomersList = new ArrayList<Customer>();
		Optional<Customer> customer = customerRepo.findById(customerNumber);
		if(customer.isPresent()){
			List<Customer> customerList = customerRepo.findAll();
			List<String> allChildCustomerNumbers = customer.get().getChildCustomerNumbers();	
			allLinkedCustomersList = customerList.stream().filter(c -> customerNumberFoundInAllChildList(c,allChildCustomerNumbers)).collect(Collectors.toList());
		}
		return ResponseEntity.ok(allLinkedCustomersList);
	}
	
	private static boolean customerNumberFoundInAllChildList(Customer customer, List<String> allChildCustomerNumbers) {
		return allChildCustomerNumbers.stream().filter(l -> l.equals(customer.getCustomerNumber())).findAny().isPresent();
	}

	@Override
	public ResponseEntity<Object> deleteByCustomerNumber(String customerNumber) {
		customerRepo.deleteById(customerNumber);
		return ResponseEntity.ok(HttpStatus.NO_CONTENT);
	}

}
