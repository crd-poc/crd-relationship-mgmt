import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GroupsComponent } from './groups/groups.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard/4711', pathMatch: 'full' },
  { 
    path: 'dashboard',    
    children : [
      {
        path: ':id',
        children: [
          {
            path: '',
            component: DashboardComponent
          },
          {
            path: 'groups',
            component: GroupsComponent
          }          
        ]
      }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
