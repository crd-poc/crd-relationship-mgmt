import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyDetailComponent }  from '../company-detail/company-detail.component';
import { FinancialAnalysisComponent }  from '../financial-analysis/financial-analysis.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	customerId : number;

  constructor(private route : ActivatedRoute) { 
  	const id = +this.route.snapshot.paramMap.get('id');
  	this.customerId = id;
  }

  ngOnInit() {
  }

}
