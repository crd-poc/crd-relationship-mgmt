package com.hcl.dfs.restapi.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hcl.dfs.restapi.model.Customer;

public interface CustomerRepo extends MongoRepository<Customer,String>{
}
