package com.hcl.dfs.restapi.main;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hcl.dfs.restapi.controller.AngularUIController;
import com.hcl.dfs.restapi.controller.CustomerController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	@Autowired
	private CustomerController customerController;
	
	@Autowired
	private AngularUIController angularUIController;
	
	@Test
	public void contextLoads() {
		assertThat(customerController).isNotNull();
		assertThat(angularUIController).isNotNull();
	}
	
}
