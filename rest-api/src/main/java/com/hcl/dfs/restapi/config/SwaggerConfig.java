package com.hcl.dfs.restapi.config;

import java.util.Collections;
import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.google.common.base.Predicates;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

	public static final Contact DEFAULT_CONTACT = new Contact("Developer (HCL UK Ltd.)", "https://www.hcl.com", "sharad_k@hcl.com");
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("HCL | POC | DFS | RestAPI", "DFS Customer API Specifications.", "V1.0", "Terms of service", DEFAULT_CONTACT,  "HCL APIs 1.0", "https://www.hcl.com/licenses/LICENSE-1.0", Collections.emptyList());

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).paths(Predicates.not(PathSelectors.regex("/error.*"))).build().genericModelSubstitutes(Optional.class).apiInfo(DEFAULT_API_INFO);
	}
}
