package com.hcl.dfs.restapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.dfs.restapi.model.Customer;
import com.hcl.dfs.restapi.service.CustomerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "Version 1.0 DFS Customer APIs")
@RestController
@CrossOrigin
@RequestMapping(value = "/v1.0/customer", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	CustomerService customerService;

	@ApiOperation(value = "Get Customer Details by {customerNumber}", response = Customer.class, notes = "API end-point to fetch the customer details based on supplied customer number.")
	@RequestMapping(value = "/{customerNumber}", method = RequestMethod.GET)
	public ResponseEntity<Customer> getCustomerById(
			@ApiParam(value = "Customer Number", required = true)
			@PathVariable("customerNumber") String customerNumber) {
		return customerService.getByCustomerNumber(customerNumber);
	}
	
	@ApiOperation(value = "Get linked Customer Details of single {customerNumber}", notes = "API end-point to fetch all the customer details based on supplied customer number.")
	@RequestMapping(value = "/{customerNumber}/allLinkedCustomers", method = RequestMethod.GET)
	public ResponseEntity<List<Customer>> getAllLinkedCustomers(
			@ApiParam(value = "Customer Number", required = true)
			@PathVariable("customerNumber") String customerNumber) {
		return customerService.getAllLinkedCustomerNumbers(customerNumber);
	}

	@ApiOperation(value = "Delete All Customers", notes = "API end-point to delete all customers from the database. Once deleted, customer data can't be restored.")
	@RequestMapping(value = "/all", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteAll() {
		return customerService.deleteAll();
	}

	@ApiOperation(value = "Delete Customer by {customerNumber}", notes = "API end-point to delete single customer record from the database by customerNumber. Once deleted, customer data can't be restored.")
	@RequestMapping(value = "/{customerNumber}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteSingleCustomer(
			@ApiParam(value = "Customer Number", required = true)
			@PathVariable("customerNumber") String customerNumber) {
		return customerService.deleteByCustomerNumber(customerNumber);
	}

	@ApiOperation(value = "Add New Customers", notes = "API end-point to create new customers. Please see the Model Schema for object structure in request payload.")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<List<Customer>> addCustomers(
			@ApiParam(value = "List of New Customers", required = true)
			@Valid @RequestBody List<Customer> newCustomers) {
		log.debug("Requested Customers : " + newCustomers.toString());
		return customerService.addCustomers(newCustomers);
	}

	@ApiOperation(value = "Get All Customers", notes = "API end-point to fetch all customers from database without any sort-order")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<List<Customer>> getAllCustomers() {
		return customerService.getAllCustomers();
	}

}
