

## Create package
mvn package

## Run the application
java -jar rest-api-0.0.1-SNAPSHOT.jar

## URL to access Swagger UI:
http://host:port/swagger-ui.html

## Sonar Qube Integration
mvn sonar:sonar -Dsonar.host.url=http://host:port

## Pre-requisite of this application
- Install MongoDb
	- Installation can be done using the brew with command "brew install mongodb".
	- Installation can be done by downloading the mongo binary zip file.