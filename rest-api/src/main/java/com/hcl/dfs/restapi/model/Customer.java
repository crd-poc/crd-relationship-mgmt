package com.hcl.dfs.restapi.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the Customer.")
public class Customer {

	@ApiModelProperty(notes = "Customer number (Autogenerated if not supplied)", required = true, position = 0)
	@Id
	private String customerNumber;
	
	@ApiModelProperty(notes = "Customer name", required = true, position = 1)
	@NotBlank
	@Size(min=1, max =500)
	private String customerName;
	
	@ApiModelProperty(notes = "Address line 1", required = true, position = 2)
	@NotBlank
	@Size(min=1, max =1000)
	private String addressLine1;
	
	@ApiModelProperty(notes = "Address line 2 (optional)", required = true, position = 3)
	private String addressLine2;
	
	@ApiModelProperty(notes = "Location / city", required = true, position = 4)
	@NotBlank
	@Size(min=1, max =300)
	private String location;
	
	@ApiModelProperty(notes = "Industry name", required = true, position = 5)
	@NotBlank
	@Size(min=1, max =500)
	private String industryName;
	
	@ApiModelProperty(notes = "Industry code", required = true, position = 6)
	@NotBlank
	@Size(min=1, max =100)
	private String industryCode;
	
	@ApiModelProperty(notes = "Industry local code", required = true, position = 7)
	@NotBlank
	@Size(min=1, max =100)
	private String industryCodeLocal;
	
	@ApiModelProperty(notes = "Industry ISIC code", required = true, position = 8)
	@NotBlank
	@Size(min=1, max =100)
	private String industryCodeISIC;
	
	@ApiModelProperty(notes = "Relationship", example = "'Corporate Parent', 'Minority Shareholder', 'Joint Venture', 'Affiliated Company', 'Property Company' & 'Majority Shareholder'", required = true, position = 9)
	@NotBlank
	@Size(min=1, max =100)
	private String relationship;

	@ApiModelProperty(notes = "Subsidiary %age.", required = true, position = 10)
	@NotBlank
	private String subsidiary;
	
	@ApiModelProperty(notes = "Branch", example = "'Group', 'Automotive', 'Leasing', 'Food', 'Sharing', 'Services' & 'Charging'", required = true, position = 11)
	@NotBlank
	@Size(min=1, max =100)
	private String branch;
	
	@ApiModelProperty(notes = "Legal form", required = true, position = 12)
	@NotBlank
	@Size(min=1, max =100)
	private String legalForm;
	
	@ApiModelProperty(notes = "Public company", example = "YES/NO", required = true, position = 13)
	@NotBlank
	@Size(min=1, max =3)
	private String publicCompany;
	
	@ApiModelProperty(notes = "Customer type", example = "'Corporate', 'Shareholder' & 'Company'", required = true, position = 14)
	@NotBlank
	@Size(min=1, max =50)
	private String customerType;
	
	@ApiModelProperty(notes = "Tax number", required = true, position = 15)
	@NotBlank
	@Size(min=1, max =50)
	private String taxNumber;
	
	@ApiModelProperty(notes = "Comment", required = true, position = 16)
	@NotBlank
	private String comment;
	
	@ApiModelProperty(notes = "Date of foundation", required = true, position = 17)
	@NotBlank
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dateOfFoundation;
	
	@ApiModelProperty(notes = "Date of business relationship", required = true, position = 18)
	@NotBlank
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date startDateOfBusinessRelationship;
	
	@ApiModelProperty(notes = "Customer number of Parent Company", required = false, position = 19)
	private String parentCustomerNumber;
	
	@ApiModelProperty(notes = "List of linked companys customer numbers.", required = false, position = 20)
	private List<String> childCustomerNumbers = new ArrayList<String>();
	
	@ApiModelProperty(notes = "Record creation date in database", required = false, position = 21)
	@CreatedDate
	private Instant createdAt;
	
	@ApiModelProperty(notes = "Record update date in database", required = false, position = 22)
	@LastModifiedDate
	private Instant lastModifiedAt;
	
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	public String getIndustryCode() {
		return industryCode;
	}
	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}
	public String getIndustryCodeLocal() {
		return industryCodeLocal;
	}
	public void setIndustryCodeLocal(String industryCodeLocal) {
		this.industryCodeLocal = industryCodeLocal;
	}
	public String getIndustryCodeISIC() {
		return industryCodeISIC;
	}
	public void setIndustryCodeISIC(String industryCodeISIC) {
		this.industryCodeISIC = industryCodeISIC;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getSubsidiary() {
		return subsidiary;
	}
	public void setSubsidiary(String subsidiary) {
		this.subsidiary = subsidiary;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getLegalForm() {
		return legalForm;
	}
	public void setLegalForm(String legalForm) {
		this.legalForm = legalForm;
	}
	public String getPublicCompany() {
		return publicCompany;
	}
	public void setPublicCompany(String publicCompany) {
		this.publicCompany = publicCompany;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public Date getDateOfFoundation() {
		return dateOfFoundation;
	}
	public void setDateOfFoundation(Date dateOfFoundation) {
		this.dateOfFoundation = dateOfFoundation;
	}
	public Date getStartDateOfBusinessRelationship() {
		return startDateOfBusinessRelationship;
	}
	public void setStartDateOfBusinessRelationship(Date startDateOfBusinessRelationship) {
		this.startDateOfBusinessRelationship = startDateOfBusinessRelationship;
	}
	public String getParentCustomerNumber() {
		return parentCustomerNumber;
	}
	public void setParentCustomerNumber(String parentCustomerNumber) {
		this.parentCustomerNumber = parentCustomerNumber;
	}
	public List<String> getChildCustomerNumbers() {
		return childCustomerNumbers;
	}
	public void setChildCustomerNumbers(List<String> childCustomerNumbers) {
		this.childCustomerNumbers = childCustomerNumbers;
	}
	public Instant getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}
	public Instant getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Instant lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public String getCustomerName() {
		return customerName;
	}
	public String getTaxNumber() {
		return taxNumber;
	}
	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "Customer [customerNumber=" + customerNumber + ", customerName=" + customerName + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", location=" + location + ", industryName=" + industryName + ", industryCode=" + industryCode + ", industryCodeLocal=" + industryCodeLocal + ", industryCodeISIC=" + industryCodeISIC + ", relationship=" + relationship + ", subsidiary=" + subsidiary + ", branch=" + branch + ", legalForm=" + legalForm + ", publicCompany=" + publicCompany + ", customerType=" + customerType + ", taxNumber=" + taxNumber + ", comment=" + comment + ", dateOfFoundation=" + dateOfFoundation + ", startDateOfBusinessRelationship=" + startDateOfBusinessRelationship + ", parentCustomerNumber=" + parentCustomerNumber + ", childCustomerNumbers=" + childCustomerNumbers + ", createdAt=" + createdAt + ", lastModifiedAt=" + lastModifiedAt + "]";
	}
}
