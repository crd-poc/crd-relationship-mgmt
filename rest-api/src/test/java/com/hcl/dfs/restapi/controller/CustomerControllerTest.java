package com.hcl.dfs.restapi.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.hcl.dfs.restapi.model.Customer;
import com.hcl.dfs.restapi.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CustomerControllerTest {

	@LocalServerPort
	private int port;

	@MockBean
	private CustomerService customerService;

	@Autowired
	private TestRestTemplate restTemplate;
	
	private Map<String, Customer> customerData = new HashMap<String, Customer>();
	private List<Customer> allCustomers = new ArrayList<Customer>();

	@Before
	public void setupTestCase() {
		setupCustomerTestData();
		when(customerService.getByCustomerNumber("10815")).thenReturn(ResponseEntity.ok(customerData.get("10815")));
		when(customerService.getByCustomerNumber("4711")).thenReturn(ResponseEntity.ok(customerData.get("4711")));
		when(customerService.getAllLinkedCustomerNumbers("4711")).thenReturn(ResponseEntity.ok(allCustomers));
		when(customerService.deleteAll()).thenReturn(ResponseEntity.ok(HttpStatus.NO_CONTENT));
		when(customerService.deleteByCustomerNumber("10815")).thenReturn(ResponseEntity.ok(HttpStatus.NO_CONTENT));
		URI customerLocation = fromUriString("/customer/all").build().toUri();
		when(customerService.addCustomers(allCustomers)).thenReturn(ResponseEntity.created(customerLocation).body(allCustomers));
		when(customerService.getAllCustomers()).thenReturn(ResponseEntity.ok(allCustomers));
	}
	
	private void setupCustomerTestData(){
		Customer customer1 = new Customer();
		customer1.setCustomerName("BMW");
		customer1.setCustomerNumber("10815");
		customer1.setParentCustomerNumber("4711");
		customer1.setAddressLine1("Munchen");
		customer1.setAddressLine2("Germany");
		customer1.setBranch("Leasing");
		customer1.setComment("This is the comment for BMW Company");
		customer1.setIndustryName("Manufacture of Motor Vehicle, trailers and semi-trailers");
		customer1.setCustomerType("Shareholder");
		customer1.setIndustryCode("34");
		customer1.setIndustryCodeISIC("41542");
		customer1.setIndustryCodeLocal("41542");
		customer1.setLegalForm("BMW");
		customer1.setLocation("Munchen");
		customer1.setPublicCompany("NO");
		customer1.setRelationship("Joint Venture");
		customer1.setSubsidiary("5%");
		customer1.setTaxNumber("51425");
		customer1.getChildCustomerNumbers().add("51425");
		customerData.put("10815", customer1);
		allCustomers.add(customer1);
		
		Customer customer2 = new Customer();
		customer2.setCustomerName("DriveNow");
		customer2.setCustomerNumber("51425");
		customer2.setParentCustomerNumber("10815");
		customer2.setIndustryName("Service");
		customer2.setAddressLine1("Munchen");
		customer2.setAddressLine2("Germany");
		customer2.setBranch("Sharing");
		customer2.setComment("This is the comment for the company DriveNow with company number 51425");
		customer2.setCustomerType("Shareholder");
		customer2.setIndustryCode("40");
		customer2.setIndustryCodeISIC("96245");
		customer2.setIndustryCodeLocal("96245");
		customer2.setLegalForm("DriveNow");
		customer2.setLocation("Munich");
		customer2.setPublicCompany("NO");
		customer2.setRelationship("Minority Shareholder");
		customer2.setSubsidiary("50%");
		customer2.setTaxNumber("96245");
		customerData.put("51425", customer2);
		allCustomers.add(customer2);
		
		Customer customer3 = new Customer();
		customer3.setCustomerName("Hans Mueller");
		customer3.setCustomerNumber("8497364376");
		customer3.setParentCustomerNumber("4711");
		customer3.setIndustryName("Mining of metal ores");
		customer3.setAddressLine1("Berlin");
		customer3.setAddressLine2("Germany");
		customer3.setBranch("Sharing");
		customer3.setComment("This is the sample comment for the company - Hans Mueller");
		customer3.setCustomerType("Corporate");
		customer3.setIndustryCode("13");
		customer3.setIndustryCodeISIC("4568");
		customer3.setIndustryCodeLocal("4568");
		customer3.setLegalForm("GmbH");
		customer3.setLocation("Berlin");
		customer3.setPublicCompany("NO");
		customer3.setRelationship("Majority Shareholder");
		customer3.setSubsidiary("60%");
		customer3.setTaxNumber("8888");
		customerData.put("8497364376", customer3);
		allCustomers.add(customer3);
		
		Customer customer4 = new Customer();
		customer4.setCustomerName("Muster GmbH");
		customer4.setCustomerNumber("4711");
		customer4.setIndustryName("Group of Holding Companies");
		customer4.setAddressLine1("Slovakia");
		customer4.setAddressLine2("Germany");
		customer4.setBranch("Group");
		customer4.setComment("This is the sample comment for the company - Muster GmbH");
		customer4.setCustomerType("Corporate");
		customer4.setIndustryCode("49410");
		customer4.setIndustryCodeISIC("49410");
		customer4.setIndustryCodeLocal("49410");
		customer4.setLegalForm("Group");
		customer4.setLocation("Slovakia");
		customer4.setPublicCompany("NO");
		customer4.setRelationship("Corporate Parent");
		customer4.setSubsidiary("100%");
		customer4.setTaxNumber("9999");
		customer4.getChildCustomerNumbers().add("8497364376");
		customer4.getChildCustomerNumbers().add("10815");
		customerData.put("8497364376", customer4);
		allCustomers.add(customer4);
	}

	@Test
	public void testGetCustomerById() {
		String expectedResultOfAPI = "{\"customerNumber\":\"10815\",\"customerName\":\"BMW\",\"addressLine1\":\"Munchen\",\"addressLine2\":\"Germany\",\"location\":\"Munchen\",\"industryName\":\"Manufacture of Motor Vehicle, trailers and semi-trailers\",\"industryCode\":\"34\",\"industryCodeLocal\":\"41542\",\"industryCodeISIC\":\"41542\",\"relationship\":\"Joint Venture\",\"subsidiary\":\"5%\",\"branch\":\"Leasing\",\"legalForm\":\"BMW\",\"publicCompany\":\"NO\",\"customerType\":\"Shareholder\",\"taxNumber\":\"51425\",\"comment\":\"This is the comment for BMW Company\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"4711\",\"childCustomerNumbers\":[\"51425\"],\"createdAt\":null,\"lastModifiedAt\":null}";
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/v1.0/customer/10815",String.class)).isEqualTo(expectedResultOfAPI);
	}
	
	@Test
	public void testGetAllLinkedCustomers() {
		String expectedResultOfAPI = "[{\"customerNumber\":\"10815\",\"customerName\":\"BMW\",\"addressLine1\":\"Munchen\",\"addressLine2\":\"Germany\",\"location\":\"Munchen\",\"industryName\":\"Manufacture of Motor Vehicle, trailers and semi-trailers\",\"industryCode\":\"34\",\"industryCodeLocal\":\"41542\",\"industryCodeISIC\":\"41542\",\"relationship\":\"Joint Venture\",\"subsidiary\":\"5%\",\"branch\":\"Leasing\",\"legalForm\":\"BMW\",\"publicCompany\":\"NO\",\"customerType\":\"Shareholder\",\"taxNumber\":\"51425\",\"comment\":\"This is the comment for BMW Company\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"4711\",\"childCustomerNumbers\":[\"51425\"],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"51425\",\"customerName\":\"DriveNow\",\"addressLine1\":\"Munchen\",\"addressLine2\":\"Germany\",\"location\":\"Munich\",\"industryName\":\"Service\",\"industryCode\":\"40\",\"industryCodeLocal\":\"96245\",\"industryCodeISIC\":\"96245\",\"relationship\":\"Minority Shareholder\",\"subsidiary\":\"50%\",\"branch\":\"Sharing\",\"legalForm\":\"DriveNow\",\"publicCompany\":\"NO\",\"customerType\":\"Shareholder\",\"taxNumber\":\"96245\",\"comment\":\"This is the comment for the company DriveNow with company number 51425\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"10815\",\"childCustomerNumbers\":[],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"8497364376\",\"customerName\":\"Hans Mueller\",\"addressLine1\":\"Berlin\",\"addressLine2\":\"Germany\",\"location\":\"Berlin\",\"industryName\":\"Mining of metal ores\",\"industryCode\":\"13\",\"industryCodeLocal\":\"4568\",\"industryCodeISIC\":\"4568\",\"relationship\":\"Majority Shareholder\",\"subsidiary\":\"60%\",\"branch\":\"Sharing\",\"legalForm\":\"GmbH\",\"publicCompany\":\"NO\",\"customerType\":\"Corporate\",\"taxNumber\":\"8888\",\"comment\":\"This is the sample comment for the company - Hans Mueller\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"4711\",\"childCustomerNumbers\":[],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"4711\",\"customerName\":\"Muster GmbH\",\"addressLine1\":\"Slovakia\",\"addressLine2\":\"Germany\",\"location\":\"Slovakia\",\"industryName\":\"Group of Holding Companies\",\"industryCode\":\"49410\",\"industryCodeLocal\":\"49410\",\"industryCodeISIC\":\"49410\",\"relationship\":\"Corporate Parent\",\"subsidiary\":\"100%\",\"branch\":\"Group\",\"legalForm\":\"Group\",\"publicCompany\":\"NO\",\"customerType\":\"Corporate\",\"taxNumber\":\"9999\",\"comment\":\"This is the sample comment for the company - Muster GmbH\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":null,\"childCustomerNumbers\":[\"8497364376\",\"10815\"],\"createdAt\":null,\"lastModifiedAt\":null}]";
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/v1.0/customer/4711/allLinkedCustomers",String.class)).isEqualTo(expectedResultOfAPI);
	}
	
	@Test
	public void testDeleteAll() {
		String deleteUrl = "http://localhost:" + port + "/v1.0/customer/all";
		this.restTemplate.delete(deleteUrl);
		ResponseEntity<Object> result = restTemplate.exchange(deleteUrl, HttpMethod.DELETE, HttpEntity.EMPTY, Object.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo("NO_CONTENT");
	}
	
	@Test
	public void testDeleteSingleCustomer() {
		String deleteUrl = "http://localhost:" + port + "/v1.0/customer/10815";
		this.restTemplate.delete(deleteUrl);
		ResponseEntity<Object> result = restTemplate.exchange(deleteUrl, HttpMethod.DELETE, HttpEntity.EMPTY, Object.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo("NO_CONTENT");
	}
	
	@Test
	public void testAddCustomers() {
		String url = "http://localhost:" + port + "/v1.0/customer/add";
		ResponseEntity<Object> result = this.restTemplate.postForEntity(url, allCustomers, Object.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testGetAllCustomers() {
		String expectedResultOfAPI = "[{\"customerNumber\":\"10815\",\"customerName\":\"BMW\",\"addressLine1\":\"Munchen\",\"addressLine2\":\"Germany\",\"location\":\"Munchen\",\"industryName\":\"Manufacture of Motor Vehicle, trailers and semi-trailers\",\"industryCode\":\"34\",\"industryCodeLocal\":\"41542\",\"industryCodeISIC\":\"41542\",\"relationship\":\"Joint Venture\",\"subsidiary\":\"5%\",\"branch\":\"Leasing\",\"legalForm\":\"BMW\",\"publicCompany\":\"NO\",\"customerType\":\"Shareholder\",\"taxNumber\":\"51425\",\"comment\":\"This is the comment for BMW Company\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"4711\",\"childCustomerNumbers\":[\"51425\"],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"51425\",\"customerName\":\"DriveNow\",\"addressLine1\":\"Munchen\",\"addressLine2\":\"Germany\",\"location\":\"Munich\",\"industryName\":\"Service\",\"industryCode\":\"40\",\"industryCodeLocal\":\"96245\",\"industryCodeISIC\":\"96245\",\"relationship\":\"Minority Shareholder\",\"subsidiary\":\"50%\",\"branch\":\"Sharing\",\"legalForm\":\"DriveNow\",\"publicCompany\":\"NO\",\"customerType\":\"Shareholder\",\"taxNumber\":\"96245\",\"comment\":\"This is the comment for the company DriveNow with company number 51425\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"10815\",\"childCustomerNumbers\":[],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"8497364376\",\"customerName\":\"Hans Mueller\",\"addressLine1\":\"Berlin\",\"addressLine2\":\"Germany\",\"location\":\"Berlin\",\"industryName\":\"Mining of metal ores\",\"industryCode\":\"13\",\"industryCodeLocal\":\"4568\",\"industryCodeISIC\":\"4568\",\"relationship\":\"Majority Shareholder\",\"subsidiary\":\"60%\",\"branch\":\"Sharing\",\"legalForm\":\"GmbH\",\"publicCompany\":\"NO\",\"customerType\":\"Corporate\",\"taxNumber\":\"8888\",\"comment\":\"This is the sample comment for the company - Hans Mueller\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":\"4711\",\"childCustomerNumbers\":[],\"createdAt\":null,\"lastModifiedAt\":null},{\"customerNumber\":\"4711\",\"customerName\":\"Muster GmbH\",\"addressLine1\":\"Slovakia\",\"addressLine2\":\"Germany\",\"location\":\"Slovakia\",\"industryName\":\"Group of Holding Companies\",\"industryCode\":\"49410\",\"industryCodeLocal\":\"49410\",\"industryCodeISIC\":\"49410\",\"relationship\":\"Corporate Parent\",\"subsidiary\":\"100%\",\"branch\":\"Group\",\"legalForm\":\"Group\",\"publicCompany\":\"NO\",\"customerType\":\"Corporate\",\"taxNumber\":\"9999\",\"comment\":\"This is the sample comment for the company - Muster GmbH\",\"dateOfFoundation\":null,\"startDateOfBusinessRelationship\":null,\"parentCustomerNumber\":null,\"childCustomerNumbers\":[\"8497364376\",\"10815\"],\"createdAt\":null,\"lastModifiedAt\":null}]";
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/v1.0/customer/all",String.class)).isEqualTo(expectedResultOfAPI);
	}

}
